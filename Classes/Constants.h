//
//  Constants.h
//  TradieCom
//
//  Created by Ravi on 1/20/15.
//  Copyright (c) 2015 ___SHARPMOTTO___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "CalendarUIAppDelegate.h"


#define RESULT_PAGE_SIZE    10
#define TextfieldFontSize 15
#define USE_JSON_FOR_TESTING NO

#define kSessionToken @"kSessionToken"
#define kUserID @"kUserID"
#define kUDIDKey @"kUDIDKey"

#define invalidEmail @"email"
#define invalidPassword @"password"
#define invalidRecoveryEmail @"login_or_email"
#define invalidAlternateEmail @"alternate_email"
#define invalidName @"name"
#define invalidPhoneNumber @"phone_no"
#define invalidServiceType @"service_type_id"


#define invalidFirstName @"Profile_firstname"

#define invalidSuburb @"JobMaster_Suburb"
#define invalidPostCode @"Postcode"

#define cellLabelPrimaryColor @"30363e"
#define bodyTextColor @"FFFFFF"
#define cellLabelSecondaryColor @"6f7883"


#define USER_DEFAULT [NSUserDefaults standardUserDefaults]

extern NSString * const RequestTypeGET;//          @"GET"
extern NSString * const RequestTypePOST;//         @"POST"

#define isLoggedInKey @"isLoggedIn"
#define isProfileComplete @"isProfileComplete"


#ifdef DEBUG
#	define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#	define DLog(...)
#endif

// ALog always displays output regardless of the DEBUG setting
#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

@interface Constants : NSObject

//setting up font with recommended size
+(UIFont*)regularFontWithSize:(float)size;
+(UIFont*)lightFontWithSize:(float)size;
+(UIFont*)boldFontWithSize:(float)size;
+(UIFont*)semiboldFontWithSize:(float)size;

+(NSString *)urlEncodeValue:(NSString *)str;
+(NSString *)baseURL;

+(CalendarUIAppDelegate*)getAppDelegate;
+(void)showAlert:(NSString*)title withMesssage:(NSString*)message;
+(NSString *)replaceIllegalCharacters:(NSString *)str;
+(NSString *)getAppVersion;
+(NSString *)prepareFlatTextFromHTML:(NSString *)html;
+(UIImage *)resizeImage:(UIImage *)image toWidth:(NSInteger)width toHeight:(NSInteger)height;
+(BOOL)isRetinaViewCompatible;
+(NSDate *)converStringToDate:(NSString*)dateString;
+(NSString*)removeEscapeSequence:(NSString*)str;
+(UIImage *)loadImage:(NSString *)name;
+(BOOL)checkConnection;
+(BOOL)isEmptyString:(NSString *)string;


+ (BOOL) addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
+ (NSString *) galleryImageNameWithJobId:(NSInteger)jobId;
+ (UIImage *) imageByNormalizingOrientation:(UIImage *)initialImage;
+ (UIImage *) loadGallaryImageWithName:(NSString *)imageName;
+ (BOOL) saveGalleryImage:(UIImage *)image withName:(NSString *)imageName;
+ (void) removeGalleryImageWithName:(NSString *)imageName;

@end

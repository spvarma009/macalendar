//
//  Constants.m
//  TradieCom
//
//  Created by Ravi on 1/20/15.
//  Copyright (c) 2015 ___SHARPMOTTO___. All rights reserved.
//

#import "Constants.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
#import "Reachability.h"
#import "NSDateFormatter+Helper.h"

NSString *key = @"Eb_G1PvbpiFPpccTO3CTA8KPlnc=";

static inline double radians (double degrees) {return degrees * M_PI/180;}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"
@implementation UITextField (custom)
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 8,
                      bounds.size.width - 20, bounds.size.height - 16);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}
@end
#pragma clang diagnostic pop

@implementation Constants

/**
 ** return URL encoded string by adding percent escape
 **/

+(NSString *)baseURL {
    return @"http://92.222.113.215:85/tradiecom/webservices/Default/WebServices/key/";
    return @"http://auratechmind.co.in/tradiecom/webservices/Default/WebServices/key/";
}
+(NSString *)urlEncodeValue:(NSString *)str
{
    NSString *result = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR("!*'();:@&=+$,/?%#[]- |_"), kCFStringEncodingUTF8));
    return result;
}

/**
 ** returns application version as a string
 **/
+(NSString *)getAppVersion{
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

+(CalendarUIAppDelegate*)getAppDelegate
{
	return (CalendarUIAppDelegate*)[[UIApplication sharedApplication] delegate];
}

+(UIFont*)regularFontWithSize:(float)size{
    return [UIFont fontWithName:@"ProximaNova-Regular" size:size];
}

+(UIFont*)lightFontWithSize:(float)size{
    return [UIFont fontWithName:@"ProximaNova-Light" size:size];
}

+(UIFont*)boldFontWithSize:(float)size{
    return [UIFont fontWithName:@"ProximaNova-Bold" size:size];
}

+(UIFont*)semiboldFontWithSize:(float)size{
    return [UIFont fontWithName:@"ProximaNova-Semibold" size:size];
}

+(void) showAlert:(NSString*)title withMesssage:(NSString*)message{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
}

+(NSDate *)converStringToDate:(NSString*)dateString{

	NSDateFormatter *df = [NSDateFormatter sharedFormatter];
	[df setDateFormat:@"EEE MMM dd, yyyy"];
	NSDate *myDate = [df dateFromString:dateString];
	return myDate;
}

/**
 Function to remove illegal characters
 **/
+(NSString *)replaceIllegalCharacters:(NSString *)str {
	NSString *ampRemoved = [str stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
	NSString *nbspRemoved = [ampRemoved stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
	NSString *quoteRemoved = [nbspRemoved stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
	NSString *doubleQuoteRemoved = [quoteRemoved stringByReplacingOccurrencesOfString:@"&ldquo;" withString:@"\""];
	NSString *doubleQuoteRemoved1 = [doubleQuoteRemoved stringByReplacingOccurrencesOfString:@"&rdquo;" withString:@"\""];
	NSString *doubleQuoteRemoved2 = [doubleQuoteRemoved1 stringByReplacingOccurrencesOfString:@"&rsquo;" withString:@"\'"];
	return [doubleQuoteRemoved2 stringByReplacingOccurrencesOfString:@"&#39;" withString:@"\'"];
}

/**
 Method used for removing the html tags from the string
 **/
+ (NSString *)prepareFlatTextFromHTML:(NSString *)html {
	NSScanner *theScanner = [NSScanner scannerWithString:html];
	NSString *text = nil;
	while ([theScanner isAtEnd] == NO) {
		[theScanner scanUpToString:@"<" intoString:NULL] ;
		[theScanner scanUpToString:@">" intoString:&text] ;
		html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
	}
	html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	//DLog(@"-----------------------------------%d",html.length);
	return html;
}

/**
 Function to resize image to new height and new width
 **/
+(UIImage *)resizeImage:(UIImage *)image toWidth:(NSInteger)width toHeight:(NSInteger)height{
	
	CGImageRef imageRef = [image CGImage];
	CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
	CGColorSpaceRef colorSpaceInfo = CGColorSpaceCreateDeviceRGB();
	
	if (alphaInfo == kCGImageAlphaNone)
		alphaInfo = kCGImageAlphaNoneSkipLast;
	
	CGContextRef bitmap = nil;
	
	if (image.imageOrientation == UIImageOrientationUp || image.imageOrientation == UIImageOrientationDown) {
		bitmap = CGBitmapContextCreate(NULL, width, height, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, (CGBitmapInfo)alphaInfo);
		
	} else {
		bitmap = CGBitmapContextCreate(NULL, height, width, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, (CGBitmapInfo)alphaInfo);
	}
	
	if(bitmap == nil){
		// don't proceedd ahead if we failed to create the bitmap context
		// return the image as it is..modified by Ravi as sometimes due to
		// invalid context images were black.
		CGColorSpaceRelease(colorSpaceInfo);
		return image;
	}
	
	if (image.imageOrientation == UIImageOrientationLeft) {
		CGContextRotateCTM (bitmap, radians(90));
		CGContextTranslateCTM (bitmap, 0, -height);
		
	} else if (image.imageOrientation == UIImageOrientationRight) {
		CGContextRotateCTM (bitmap, radians(-90));
		CGContextTranslateCTM (bitmap, -width, 0);
		
	} else if (image.imageOrientation == UIImageOrientationUp) {
		
	} else if (image.imageOrientation == UIImageOrientationDown) {
		CGContextTranslateCTM (bitmap, width,height);
		CGContextRotateCTM (bitmap, radians(-180.));
		
	}
	
	CGContextDrawImage(bitmap, CGRectMake(0, 0, width, height), imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(bitmap);
	UIImage *result = [UIImage imageWithCGImage:ref];
	
	CGColorSpaceRelease(colorSpaceInfo);
	CGContextRelease(bitmap);
	CGImageRelease(ref);
	
	return result;
}

/**
 Return true/false by checking whether user device is compatible for 2x (retina view) display or not
 **/
+(BOOL)isRetinaViewCompatible{
	return ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2);
}

+(NSString*)removeEscapeSequence:(NSString*)str{
	return nil;
}

+(UIImage *)loadImage:(NSString *)name {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:name];
	UIImage *img = [UIImage imageWithContentsOfFile:fullPath];
	
	return img;
}

+(BOOL)checkConnection{
	// Observe the kNetworkReachabilityChangedNotification. When that notification is posted, the
    // method "reachabilityChanged" will be called.
    //[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
	//Instace variable related with reachability
    Reachability* internetReach = [Reachability reachabilityForInternetConnection];
	NetworkStatus remoteHostStatus = [internetReach currentReachabilityStatus];
	if(remoteHostStatus == NotReachable)
    {
		return FALSE;
	}
	return TRUE;
}

// Returns YES if the string is nil or equal to @""
+ (BOOL)isEmptyString:(NSString *)string{
    // Note that [string length] == 0 can be false when [string isEqualToString:@""] is true, because these are Unicode strings.
    
    if (((NSNull *) string == [NSNull null]) || (string == nil) ) {
        return YES;
    }
    string = [string stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([string isEqualToString:@""]) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    
    return success;
    
    /*const char* filePath = [[URL path] fileSystemRepresentation];
     
     const char* attrName = "com.apple.MobileBackup";
     u_int8_t attrValue = 1;
     
     int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
     return result == 0;*/
}

+ (NSString *) galleryImageNameWithJobId:(NSInteger)jobId
{
    NSString *imageName = [NSString stringWithFormat:@"IMG_%ld_", (long)jobId];
    
    NSDateFormatter *dateFormat = [NSDateFormatter sharedFormatter];
    [dateFormat setDateFormat:@"ddMMyyyy_HHmmss"];
    NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
    
    return [NSString stringWithFormat:@"%@%@", imageName, dateString];
}

+ (UIImage *) imageByNormalizingOrientation:(UIImage *)initialImage
{
    if (initialImage.imageOrientation == UIImageOrientationUp)
        return initialImage;
    
    CGSize size = initialImage.size;
    UIGraphicsBeginImageContextWithOptions(size, NO, initialImage.scale);
    [initialImage drawInRect:(CGRect){{0, 0}, size}];
    UIImage* normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return normalizedImage;
}

+ (NSString *) getImageDirectoryPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *dirName = @"Images";
    NSString *destinationPath = [documentsDirectory stringByAppendingPathComponent:dirName];
    
    NSError *error = nil;
    if (![fileManager fileExistsAtPath:destinationPath]) {
        [fileManager createDirectoryAtPath:destinationPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
        [Constants addSkipBackupAttributeToItemAtURL:[[NSURL alloc] initFileURLWithPath:destinationPath isDirectory:YES]];
    }
    
    return destinationPath;
}

+ (UIImage *) loadGallaryImageWithName:(NSString *)imageName
{
    NSString *fullPath = [[Constants getImageDirectoryPath] stringByAppendingPathComponent:imageName];
    UIImage *image = [UIImage imageWithContentsOfFile:fullPath];
    
    return image;
}

+ (BOOL) saveGalleryImage:(UIImage *)image withName:(NSString *)imageName
{
    NSData *data = UIImageJPEGRepresentation(image, 0.5);
    
    NSString *fullPath = [[Constants getImageDirectoryPath] stringByAppendingPathComponent:imageName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
    if (!fileExists) {
        [[NSFileManager defaultManager] createFileAtPath:fullPath contents:data attributes:nil];
    }
    
    
    return !fileExists;
}

+ (void) removeGalleryImageWithName:(NSString *)imageName
{
    NSString *fullPath = [[Constants getImageDirectoryPath] stringByAppendingPathComponent:imageName];
    [[NSFileManager defaultManager] removeItemAtPath:fullPath error:nil];
}

@end

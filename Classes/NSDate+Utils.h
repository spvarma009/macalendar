//
//  NSDate+Utils.h
//  WHVVConsumer
//
//  Created by Anton Bremer on 20-02-14.
//  Copyright (c) 2014 Mobiquity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

+ (NSDate *)oneDayBefore:(NSDate *)date;
+ (NSDate *)oneDayAfter:(NSDate *)date;
+ (NSDate *)numberOfDays:(NSInteger)numberOfDays from:(NSDate *)fromDate;
+ (NSDate *)clearedDate:(NSDate *)date;
- (BOOL)isToday;
- (BOOL)isTomorrow;
- (BOOL)isBefore:(NSDate *)date;
- (BOOL)isAfter:(NSDate *)date;
- (BOOL)isInFuture;
- (BOOL)isInPast;


@end

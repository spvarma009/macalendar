//
//  NSDate+Utils.m
//  TradieCom
//
//  Created by Suresh Varma on 30-05-15.
//  Copyright (c) 2015 Mobiquity. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)

+ (NSDate *)oneDayBefore:(NSDate *)date {
    return [NSDate numberOfDays:-1 from:date];
}

+ (NSDate *)oneDayAfter:(NSDate *)date {
    return [NSDate numberOfDays:1 from:date];
}
+ (NSDate *)oneHourAfter:(NSDate *)date {
    NSDateComponents *hourComponent = [[NSDateComponents alloc] init];
    hourComponent.hour = 1;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:hourComponent toDate:date options:0];
}

+ (NSDate *)addHour:(NSInteger)hours toDate:(NSDate *)date {
    NSDateComponents *hourComponent = [[NSDateComponents alloc] init];
    hourComponent.hour = hours;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:hourComponent toDate:date options:0];
}

+ (NSDate *)numberOfDays:(NSInteger)numberOfDays from:(NSDate *)fromDate {
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = numberOfDays;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:dayComponent toDate:fromDate options:0];
}

+ (NSDate *)clearedDate:(NSDate *)date {
    NSDateComponents *dateComponent = [[NSDateComponents alloc] init];
    dateComponent.timeZone = [NSTimeZone localTimeZone];
    dateComponent.hour = 0;
    dateComponent.minute = 0;
    dateComponent.second = 0;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:dateComponent toDate:date options:0];
}

- (BOOL)isOnTheSameDayAs:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components1 = [calendar components:(NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit) fromDate:self];
	NSDateComponents *components2 = [calendar components:NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit fromDate:date];
    
	return ((components1.year == components2.year) &&
			(components1.month == components2.month) &&
			(components1.day == components2.day));
}

- (BOOL)isToday
{
	return [self isOnTheSameDayAs:[NSDate date]];
}

- (BOOL)isTomorrow
{
    NSDate *tomorrow = [NSDate oneDayAfter:[NSDate date]];
	return [self isOnTheSameDayAs:tomorrow];
}

- (BOOL)isBefore:(NSDate *)date
{
	return ([self compare:date] == NSOrderedAscending);
}

- (BOOL)isAfter:(NSDate *)date
{
	return ([self compare:date] == NSOrderedDescending);
}

- (BOOL)isInFuture
{
    return ([self isAfter:[NSDate date]]);
}

- (BOOL)isInPast
{
    return ([self isBefore:[NSDate date]]);
}

@end

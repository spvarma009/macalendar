//
//  NSDateFormatter+Helper.h
//  TradieCom
//
//  Created by Suresh Varma on 21/03/15.
//  Copyright (c) 2015 ___SHARPMOTTO___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Helper)

+ (NSDateFormatter *)sharedFormatter;

@end

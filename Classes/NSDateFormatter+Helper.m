//
//  NSDateFormatter+Helper.m
//  TradieCom
//
//  Created by Suresh Varma on 21/03/15.
//  Copyright (c) 2015 ___SHARPMOTTO___. All rights reserved.
//


#import "NSDateFormatter+Helper.h"

@implementation NSDateFormatter (Helper)

+ (NSDateFormatter *)sharedFormatter
{
    static dispatch_once_t onceToken;
    static NSDateFormatter * _instance;
    dispatch_once(&onceToken, ^{
        _instance = [[self class] new];
        _instance.timeZone = [NSTimeZone localTimeZone];
    });
    
    return _instance;
}

@end
